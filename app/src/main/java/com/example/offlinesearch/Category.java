package com.example.offlinesearch;

import java.util.List;

/**
 * Created by avinash.r on 20/07/16.
 */
public class Category {

    private long createdAt;
    private long updatedAt;
    private String title;
    private List<Item> products;

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Item> getProducts() {
        return products;
    }

    public void setProducts(List<Item> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Category{" +
                "createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", title='" + title + '\'' +
                ", products=" + products +
                '}';
    }
}
