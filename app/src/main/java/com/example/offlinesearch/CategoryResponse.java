package com.example.offlinesearch;

import java.util.List;

/**
 * Created by avinash.r on 20/07/16.
 */
public class CategoryResponse {

    List<Category> response;

    public List<Category> getResponse() {
        return response;
    }

    public void setResponse(List<Category> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "response=" + response +
                '}';
    }
}
