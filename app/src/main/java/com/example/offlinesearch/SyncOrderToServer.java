package com.example.offlinesearch;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.offlinesearch.lucene.LuceneSearchUtil;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.apache.lucene.document.Document;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by anuj.chaudhary on 21/07/16.
 */
public class SyncOrderToServer extends AsyncTask<String, Void, List<Integer>> {
    private WeakReference<List<Order>> orders;
    private Boolean updateFlag;


    public SyncOrderToServer(List<Order> orders, Boolean updateFlag) {
        this.orders = new WeakReference<List<Order>>(orders);
        this.updateFlag = updateFlag;
    }

    public SyncOrderToServer() {}

    @Override
    protected List<Integer> doInBackground(String... params) {

        try {
            URL u = new URL(params[0]);

            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");

            conn.connect();
            InputStream is = conn.getInputStream();

// Read the stream
            byte[] b = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            while ( is.read(b) != -1)
                baos.write(b);

            String JSONResp = new String(baos.toByteArray());
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(JSONResp));
            reader.setLenient(true);
            OrderResponse response = gson.fromJson(reader, OrderResponse.class);

            return response.getResponse();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(List<Integer> result) {

        if (updateFlag)
            updateOrder(result);
        else {
            if (result != null)
                createOrder(result);
            else {
                Order order = orders.get().get(0);
                order.setStatus("IN_SYNC");
                try {
                    LuceneSearchUtil.insertOrders(order);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createOrder(List<Integer> result) {
        if (result.get(0) == 0) {
            try {
                LuceneSearchUtil.insertOrders(orders.get().get(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateOrder(List<Integer> result) {
        int i = 0;
        for (Order order : orders.get()) {

            Log.i("order", order.toString());
            Log.i("server", result.get(i).toString());

            switch (result.get(i++)) {
                case 0:
                    order.setStatus("SUCCESS");
                    break;
                case 1:
                    order.setStatus("PRICE MISMATCHED");
                    break;
                case 2:
                    order.setStatus("ITEM OUT OF STOCK");
            }

            //update lucene
            try {
                LuceneSearchUtil.updateDocuments("orderId",order.getOrderId(), order);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
