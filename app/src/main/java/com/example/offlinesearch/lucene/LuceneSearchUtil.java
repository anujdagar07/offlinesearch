package com.example.offlinesearch.lucene;

import android.util.Log;

import com.example.offlinesearch.Category;
import com.example.offlinesearch.Item;
import com.example.offlinesearch.Order;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.spans.SpanMultiTermQueryWrapper;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by avinash.r on 01/07/16.
 */
public class LuceneSearchUtil {

    private static final String tag = LuceneSearchUtil.class.getName();
    public static List<Object> list = new ArrayList();

    public LuceneSearchUtil() {

    }

    public static void insertOrders(Order order) throws IOException {
        IndexWriter writer = Indexer.getWriter();
        writer.addDocument(createDocument(order));
        Indexer.commit();
    }


    public static void insertDocuments(Category result) throws IOException {
        IndexWriter writer = Indexer.getWriter();
        for (Item item : result.getProducts()) {
            Document document = new Document();
            document.add(new Field("sellingPrice", String.valueOf(item.getSellingPrice()),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("originalPrice", String.valueOf(item.getOriginalPrice()),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("color", String.valueOf(item.getColor()),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("imageUrls.main", item.getId() + "_main.jpg",
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("imageUrls.thumnail", item.getId() + "_thumnail.jpg",
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("id", String.valueOf(item.getId()),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("title", String.valueOf(item.getTitle()),
                    Field.Store.YES, Field.Index.ANALYZED));
            document.add(new Field("specifications", String.valueOf(item.getSpecifications()),
                    Field.Store.YES, Field.Index.ANALYZED));
            document.add(new Field("category", String.valueOf(result.getTitle()),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new Field("documentType", "items",
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            writer.addDocument(document);
        }
        Indexer.commit();
    }

    //update methods

    public static void updateDocuments(String key, String searchTerm, Order order) throws IOException {
        IndexWriter writer = Indexer.getWriter();
        writer.updateDocument(new Term(key, searchTerm), createDocument(order));
        Indexer.commit();
    }

    private static Document createDocument(Order order) {
        Document document = new Document();
        document.add(new Field("orderTitle", order.getOrderTitle(), Field.Store.YES, Field.Index.ANALYZED));
        document.add(new Field("status", order.getStatus(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        document.add(new Field("productId", order.getProductId(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        document.add(new Field("orderId", order.getOrderId(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        document.add(new Field("documentType", "order", Field.Store.YES, Field.Index.NOT_ANALYZED));
        return document;
    }


    /*
     *
     Add multiple search keys
     *
     */
    private static List<Document> searchDocuments(String key, String searchTerm,
                                                  Map<String, String> andTerms, Map<String, String> orTerms) throws IOException {
        SearcherManager searcherManager = Indexer.getSearcherManager();
        IndexSearcher searcher = searcherManager.acquire();
        //QueryParser parser = new QueryParser(Version.LUCENE_36, "name", new StandardAnalyzer(Version.LUCENE_36));
        //Query query = parser.parse("avinash");

        BooleanQuery mainQuery = new BooleanQuery();

        for (Map.Entry<String, String> andTerm : andTerms.entrySet()) {
            mainQuery.add(new TermQuery(new Term(andTerm.getKey(), andTerm.getValue())), BooleanClause.Occur.MUST);
        }

        if (!orTerms.isEmpty()) {

            BooleanQuery filter = new BooleanQuery();
            for (Map.Entry<String, String> orTerm : orTerms.entrySet()) {
                filter.add(new TermQuery(new Term(orTerm.getKey(), orTerm.getValue())), BooleanClause.Occur.SHOULD);
            }
            mainQuery.add(filter, BooleanClause.Occur.MUST);
        }

        //split on space
        String[] terms= searchTerm.split("[\\s]+");
        //multiple terms are to be searched
        SpanQuery[] spanQueryArticleTitle=new SpanQuery[terms.length];
        int i=0;
        for (String term:terms){
            //wildcardquery
            WildcardQuery fuzzyQuery=new WildcardQuery(new Term(key, term.toLowerCase() + "*"));
            spanQueryArticleTitle[i]=new SpanMultiTermQueryWrapper<WildcardQuery>(fuzzyQuery);
            i=i+1;
        }
        //no words between the typed text you could increase this but then performance will be lowered
        SpanNearQuery query = new SpanNearQuery(spanQueryArticleTitle,0,true);

        mainQuery.add(query, BooleanClause.Occur.MUST);

        TopDocs docs = searcher.search(mainQuery, 20);
        List<Document> list = new ArrayList<>();
        for(ScoreDoc doc : docs.scoreDocs) {
            list.add(searcher.doc(doc.doc));
        }
        searcher.close();
        return list;
    }

    private static List<Item> getItem(String key, String searchTerm) throws IOException {
        Map<String, String> mp = new HashMap<>();
        mp.put("documentType", "items");
        List<Document> docs = searchDocuments(key, searchTerm, mp, new HashMap<String, String>());
        List<Item> items = new ArrayList<>();
        for (Document document : docs) {
            List<String> imageurls = new ArrayList<>();
            imageurls.add(document.get("imageUrls.main"));
            imageurls.add(document.get("imageUrls.thumnail"));
            items.add(new Item(Double.parseDouble(document.get("sellingPrice")),
                    Double.parseDouble(document.get("originalPrice")),
                    document.get("color"), imageurls, document.get("id"),
                    document.get("title"), document.get("specifications")));
        }
        return items;    }

    private static List<Order> getOrder(String key, String searchTerm) throws IOException {
        Map<String, String> andTerms = new HashMap<String, String>();
        //andTerms.put("status", "IN_SYNC");
        List<Document> docs = searchDocuments(key, searchTerm, andTerms, new HashMap<String, String>());
        List<Order> orders = new ArrayList<>();
        for (Document document : docs) {
            orders.add(new Order(document.get("orderTitle"), document.get("status"), document.get("productId"),document.get("orderId")));
        }
        return orders;
    }

    public static void updateOrder(String key, String query) throws IOException {
        List<Order> updatedList = getOrder(key, query);
        list = new ArrayList<>();
        for(Order s : updatedList) {
            list.add(s);
        }
    }

    public static void updateItem(String key, String query) throws IOException {
        if(query == null)
            return;
        List<Item> updatedList = getItem(key, query);
        list = new ArrayList<>();
        for(Item s : updatedList) {
            list.add(s);
        }
    }

    public static List<Order> getPendingOrder(String key, String query) throws IOException {
        Map<String, String> andTerms = new HashMap<String, String>();
        andTerms.put("status", "IN_SYNC");
        List<Document> docs = searchDocuments(key, query, andTerms, new HashMap<String, String>());
        List<Order> orders = new ArrayList<>();
        for (Document document : docs) {
            orders.add(new Order(document.get("orderTitle"), document.get("status"), document.get("productId"),document.get("orderId")));
        }

        return orders;
//        list = new ArrayList<>();
//        for(Order s : orders) {
//            list.add(s);
//        }
    }
}
