package com.example.offlinesearch;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import com.example.offlinesearch.lucene.LuceneSearchUtil;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by avinash.r on 20/07/16.
 */
public class FetchProductsFromServer extends AsyncTask<String, Void, Category> {

    private WeakReference<ArrayAdapter<Item>> customListWeakReference;
    private boolean flag = false;

    public FetchProductsFromServer(ArrayAdapter<Item> customList) {
        this.customListWeakReference = new WeakReference<ArrayAdapter<Item>>(customList);
    }

    public FetchProductsFromServer(boolean flag) {
        this.flag = flag;
    }

    public FetchProductsFromServer() {}

    @Override
    protected Category doInBackground(String... params) {

        Category result = null;
        try {
            URL u = new URL(params[0]);

            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");

            conn.connect();
            InputStream is = conn.getInputStream();

// Read the stream
            byte[] b = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            while ( is.read(b) != -1)
                baos.write(b);

            String JSONResp = new String(baos.toByteArray());
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(JSONResp));
            reader.setLenient(true);
            CategoryResponse response = gson.fromJson(reader, CategoryResponse.class);

            return response.getResponse().get(0);
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(Category result) {
        if(OfflineSearchApplication.isOffline && flag) {
            //store to lucene whatever is coming
            try {
                LuceneSearchUtil.insertDocuments(result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(customListWeakReference != null){
            customListWeakReference.get().clear();
            customListWeakReference.get().addAll(result.getProducts());
            customListWeakReference.get().notifyDataSetChanged();
        }
    }
}
