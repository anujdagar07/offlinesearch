package com.example.offlinesearch;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.offlinesearch.lucene.LuceneSearchUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj.chaudhary on 20/07/16.
 */
public class OrderHistoryActivity extends BaseActivity {
    private ListView listView;
    private ArrayAdapter<Order> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Intent myIntent = new Intent(OrderHistoryActivity.this, ProductDetailsActivity.class);
//                String jsonLocation = null;
////                try {
//////                    jsonLocation = AssetJSONFile("product.json", OrderHistoryActivity.this);
////                    Log.i("product in Main: ", jsonLocation);
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
//                myIntent.putExtra("product", jsonLocation); //Optional parameters
//                OrderHistoryActivity.this.startActivity(myIntent);

            }
        });
        Log.e("tag", LuceneSearchUtil.list.size() + "");
        try {
            LuceneSearchUtil.updateOrder("documentType", "order");
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Order> orders = new ArrayList<>();
        for(Object txt : LuceneSearchUtil.list) {
            orders.add((Order)txt);
        }
        adapter = new CustomOrderList(this, orders);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

//dobule search
    public void updateListView(String s) throws IOException {
        LuceneSearchUtil.updateOrder("orderTitle", s);
        adapter.clear();
        List<Order> orders = new ArrayList<>();
        for(Object txt : LuceneSearchUtil.list) {
            orders.add((Order) txt);
        }
        adapter.addAll(orders);
        adapter.notifyDataSetChanged();
    }

}
