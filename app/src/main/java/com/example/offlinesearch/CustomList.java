package com.example.offlinesearch;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by avinash.r on 12/07/16.
 */
public class CustomList extends ArrayAdapter<Item> {

    private final Activity context;
    private List<Item> items;
    public CustomList(Activity context,
                      List<Item> items) {
        super(context, R.layout.list_single, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(items.get(position).getTitle());
        imageView.setImageBitmap(ProductDetailsActivity.getBitmapFromAsset(items.get(position).getId() + "_thumnail.jpg", context));

        return rowView;
    }
}
