package com.example.offlinesearch;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;

/**
 * Created by anuj.chaudhary on 21/07/16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private SearchView searchView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_view, menu);

        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSubmitButtonEnabled(true);
        searchView.setMaxWidth(1000);
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    updateListView(query);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //will do in offline search if required
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.offline:
                changeOfflineMode(item);
                return true;
            case R.id.wishlist:
                //showHelp();
                return true;
            case R.id.orders:
                BaseActivity.this.startActivity(new Intent(BaseActivity.this, OrderHistoryActivity.class));
                return true;
            case R.id.shop:
                //showHelp();
                BaseActivity.this.startActivity(new Intent(BaseActivity.this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public abstract void updateListView(String s) throws IOException;

    private void changeOfflineMode(MenuItem item) {
        OfflineSearchApplication.isOffline = !OfflineSearchApplication.isOffline;
        if(item.getTitle().toString().equals("Enable Offline")) {
            (new FetchProductsFromServer(true)).execute("http://10.0.2.2:8080/api/v1/document/search/category/Sunglasses/");
            item.setTitle("Disable Offline");
        }
        else {
            item.setTitle("Enable Offline");
        }
    }

}
