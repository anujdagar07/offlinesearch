package com.example.offlinesearch;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by anuj.chaudhary on 20/07/16.
 */
public class CustomOrderList extends ArrayAdapter<Order>{
    private final Activity context;
    private List<Order> orders;
    public CustomOrderList(Activity context,
                      List<Order> orders) {
        super(context, R.layout.my_orders_cell, orders);
        this.context = context;
        this.orders = orders;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.my_orders_cell, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.product);
        TextView status = (TextView) rowView.findViewById(R.id.status);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(orders.get(position).getOrderTitle());
        status.setText("STATUS: "+orders.get(position).getStatus());
        imageView.setImageBitmap(ProductDetailsActivity.getBitmapFromAsset(orders.get(position).getProductId() + "_thumnail.jpg", context));

//        try {
//            String[] params = {};
//            new RetrieveImage(imageView).execute(params).get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }

        //imageView.setImageResource(imageId[position]);
        return rowView;
    }
}
