package com.example.offlinesearch.lucene;

import android.os.Environment;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;

/**
 * Created by avinash.r on 09/07/16.
 */
public final class Indexer {

    private static String DIR_PATH = "/storage/sdcard/Android/data/com.example.offlinesearch/files/indexName1";
    private static IndexWriter WRITER = null;
    private static SearcherManager SEARCHER_MANAGER = null;
    private static boolean CREATED = false;

    public static IndexWriter getWriter() {
        return WRITER;
    }

    public static SearcherManager getSearcherManager() {
        return SEARCHER_MANAGER;
    }

    public static void createWriterAndSearcher() throws IOException {

        if (!CREATED) {
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);

            Directory indexDirectory =
                    FSDirectory.open(new File(Environment.getExternalStorageDirectory(),"/Android/data/com.example.offlinesearch/files/indexName1"));
            WRITER = new IndexWriter(indexDirectory, config);

            SEARCHER_MANAGER = new SearcherManager(WRITER, true, null);
        }
        CREATED = true;
    }

    public static void close() throws IOException {
        WRITER.close();
    }

    public static void commit() throws IOException {
        SEARCHER_MANAGER.maybeRefresh();
        WRITER.commit();
    }
}
