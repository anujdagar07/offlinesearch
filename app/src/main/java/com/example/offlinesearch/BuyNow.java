package com.example.offlinesearch;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.offlinesearch.lucene.LuceneSearchUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by anuj.chaudhary on 20/07/16.
 */
public class BuyNow extends AppCompatActivity {

    private TextView amount;
    private TextView productDetail;
    private Button pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_now);

        productDetail = (TextView) findViewById(R.id.textView7);
        amount = (TextView) findViewById(R.id.textView10);
        pay = (Button) findViewById(R.id.button);

        final Item product = (Item)getIntent().getSerializableExtra("product"); //if it's a string you stored.
        Log.i("product in buynow: ", product.toString());
        productDetail.setText(product.getTitle());
        amount.setText("Rs. " + product.getSellingPrice());

        this.pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("pay_now: ", "button clicked");

                Order order = new Order(product.getTitle(), "SUCCESS", product.getId(), product.getId()+"_"+System.currentTimeMillis());
                // call order place with true
                List<Order> orders = new ArrayList<Order>();
                orders.add(order);

                new SyncOrderToServer(orders,false).execute("http://10.0.2.2:8080/api/v1/opsearch/book/true/"+orders.size());


//                {
//                    //adding order to lucene while exception
//                    try {
//                        order.setStatus("IN_SYNC");
//                        LuceneSearchUtil.insertOrders(order);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }


                Toast.makeText(BuyNow.this, "Order placed", Toast.LENGTH_LONG).show();

                Intent myIntent = new Intent(BuyNow.this, OrderHistoryActivity.class);
                //myIntent.putExtra("product", jsonLocation); //Optional parameters
                BuyNow.this.startActivity(myIntent);

                finish();
            }
        });
    }

}
