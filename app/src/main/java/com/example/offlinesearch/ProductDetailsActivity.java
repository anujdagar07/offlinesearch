package com.example.offlinesearch;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.offlinesearch.lucene.LuceneSearchUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by anuj.chaudhary on 18/07/16.
 */
public class ProductDetailsActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView1;
    private TextView textView2;
    private TextView textView3;
    private ImageButton buyNow;
    private Item product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);

        imageView = (ImageView) findViewById(R.id.imageView);
        textView1 = (TextView)findViewById(R.id.textView1);
        textView2 = (TextView)findViewById(R.id.textView2);
        //textView3 = (TextView)findViewById(R.id.textView3);
        buyNow = (ImageButton) findViewById(R.id.imageButton2);

        product = (Item) getIntent().getSerializableExtra("product"); //if it's a string you stored.
        Log.i("product in Product: ", product.toString());

        textView1.setText(product.getTitle());
        textView2.setText(product.getSpecifications());
        //textView3.setText("Price: " + product.getSellingPrice());
        ((TextView) findViewById(R.id.textView4)).setText("Price: \u20B9"+String.valueOf((int)(product.getSellingPrice()+ 0.5d+new Random().nextInt(50)))+"        "+String.valueOf(new Random().nextInt(5) + 1)+ "km away");
        ((TextView) findViewById(R.id.textView5)).setText("Price: \u20B9"+String.valueOf((int)(product.getSellingPrice()+ 0.5d+new Random().nextInt(50))));
        ((TextView) findViewById(R.id.textView6)).setText("Price: \u20B9"+String.valueOf((int)(product.getSellingPrice()+ 0.5d+new Random().nextInt(50)))+"        "+String.valueOf(new Random().nextInt(5) + 1)+ "km away");
        ((TextView) findViewById(R.id.textView7)).setText("Price: \u20B9"+String.valueOf((int)(product.getSellingPrice()+ 0.5d+new Random().nextInt(50))));

        imageView.setImageBitmap(getBitmapFromAsset(product.getId() + "_main.jpg", ProductDetailsActivity.this));

        this.buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("buy_now: ", "button clicked");
                //finish();
                Intent myIntent = new Intent(ProductDetailsActivity.this, BuyNow.class);
                myIntent.putExtra("product", product); //Optional parameters
                ProductDetailsActivity.this.startActivity(myIntent);
            }
        });

//        try {
//            String[] params = {};
//            new RetrieveImage(imageView).execute(params).get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
    }

    public static Bitmap getBitmapFromAsset(String strName, Context context)
    {
        AssetManager assetManager = context.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }
}
