package com.example.offlinesearch;

import java.util.List;

/**
 * Created by anuj.chaudhary on 21/07/16.
 */
public class OrderResponse {

    List<Integer> response;

    public List<Integer> getResponse() {
        return response;
    }

    public void setResponse(List<Integer> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "response=" + response +
                '}';
    }
}
