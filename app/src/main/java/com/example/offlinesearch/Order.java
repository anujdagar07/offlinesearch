package com.example.offlinesearch;

/**
 * Created by anuj.chaudhary on 20/07/16.
 */
public class Order {
    String orderTitle;
    String status;
    String productId;
    String orderId;

    public Order(String orderTitle, String status, String productId, String orderId) {
        this.orderTitle = orderTitle;
        this.status = status;
        this.productId = productId;
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderTitle='" + orderTitle + '\'' +
                ", status='" + status + '\'' +
                ", productId='" + productId + '\'' +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}
