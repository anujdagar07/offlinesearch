package com.example.offlinesearch;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.example.offlinesearch.lucene.LuceneSearchUtil;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

/**
 * Created by anuj.chaudhary on 20/07/16.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    List<Order> orders;

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("*** Action: " + intent.getAction());
        if(intent.getAction().equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE")) {
            Toast.makeText(context, "Connection changed", Toast.LENGTH_SHORT).show();
        }
        System.out.println(isOnline(context));

        if (isOnline(context)) {
            // sync local order to server.
            sync_order();
            // push notification on success
            notifyUser(context);
        }

    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    public void sync_order() {
        try {
            orders = LuceneSearchUtil.getPendingOrder("documentType", "order");
        } catch (IOException e) {
            e.printStackTrace();
        }
        new SyncOrderToServer(orders,true).execute("http://10.0.2.2:8080/api/v1/opsearch/book/false/"+orders.size());

    }

    public void notifyUser(Context context) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");

        PendingIntent broadcast = PendingIntent.getBroadcast(context, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 3);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);

        Log.i("notification", "setting notification");
    }

}

