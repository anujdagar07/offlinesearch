package com.example.offlinesearch;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.offlinesearch.lucene.LuceneSearchUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends BaseActivity {

    private ListView listView;
    private ArrayAdapter<Item> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent myIntent = new Intent(MainActivity.this, ProductDetailsActivity.class);
                myIntent.putExtra("product", adapter.getItem(position)); //Optional parameters
                MainActivity.this.startActivity(myIntent);

            }
        });
        Log.e("tag", LuceneSearchUtil.list.size() + "");

        List<Item> items = new ArrayList<>();

        adapter = new CustomList(this, items);
        listView.setAdapter(adapter);
        try {
            updateListView("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        // Get Connectivity Manager class object from Systems Service
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get Network Info from connectivity Manager
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public void updateListView(String s) throws IOException{
        if (isNetworkAvailable())
            onlineUpdate(s);
        else {
            LuceneSearchUtil.updateItem("title", s);
            adapter.clear();
            List<Item> items = new ArrayList<>();
            for(Object txt : LuceneSearchUtil.list) {
                items.add((Item) txt);
            }
            adapter.addAll(items);
            adapter.notifyDataSetChanged();
        }
    }

    public void onlineUpdate(String s) {
        (new FetchProductsFromServer(adapter)).execute("http://10.0.2.2:8080/api/v1/document/search/category/womencloth/");
    }
}
