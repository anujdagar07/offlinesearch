package com.example.offlinesearch;

import java.io.Serializable;
import java.util.List;

/**
 * Created by avinash.r on 12/07/16.
 */
public class Item implements Serializable{


    double sellingPrice;
    double originalPrice;
    String color;
    List<String> imageurls;
    String id;
    String title;
    String specifications;

    public Item(double sellingPrice, double originalPrice, String color, List<String> imageurls, String id, String title, String specifications) {
        this.sellingPrice = sellingPrice;
        this.originalPrice = originalPrice;
        this.color = color;
        this.imageurls = imageurls;
        this.id = id;
        this.title = title;
        this.specifications = specifications;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<String> getImageurls() {
        return imageurls;
    }

    public void setImageurls(List<String> imageurls) {
        this.imageurls = imageurls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    @Override
    public String toString() {
        return "Item{" +
                "sellingPrice=" + sellingPrice +
                ", originalPrice=" + originalPrice +
                ", color='" + color + '\'' +
                ", imageurls=" + imageurls +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", specifications='" + specifications + '\'' +
                '}';
    }
}
